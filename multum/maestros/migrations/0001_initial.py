# Generated by Django 2.2.7 on 2020-01-09 03:51

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AgroArtesanal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('agro_artesanal', models.CharField(blank=True, max_length=255)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'AgroArtesanal',
                'verbose_name_plural': 'AgroArtesanales',
            },
        ),
        migrations.CreateModel(
            name='Puerto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('puerto', models.CharField(blank=True, max_length=255)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'Puerto',
                'verbose_name_plural': 'Puertos',
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('sector', models.CharField(blank=True, max_length=255)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'Sector',
                'verbose_name_plural': 'Sectores',
            },
        ),
        migrations.CreateModel(
            name='TipoFinca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('tipo_finca', models.CharField(blank=True, max_length=255)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'TipoFinca',
                'verbose_name_plural': 'TipoFincas',
            },
        ),
        migrations.CreateModel(
            name='TipoIdentificacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('tipo_identificacion', models.CharField(blank=True, max_length=50)),
                ('codigo', models.CharField(blank=True, max_length=50)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'TipoIdentificacion',
                'verbose_name_plural': 'TipoIdentificaciones',
            },
        ),
        migrations.CreateModel(
            name='TipoRol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('tipo_rol', models.CharField(blank=True, max_length=50)),
                ('codigo', models.CharField(blank=True, max_length=50)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'TipoRol',
                'verbose_name_plural': 'TipoRoles',
            },
        ),
        migrations.CreateModel(
            name='Vapor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('vapor', models.CharField(blank=True, max_length=255)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'verbose_name': 'Vapor',
                'verbose_name_plural': 'Vapores',
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('identificacion', models.CharField(blank=True, max_length=25)),
                ('primer_apellido', models.CharField(blank=True, max_length=50)),
                ('segundo_apellido', models.CharField(blank=True, max_length=50)),
                ('primer_nombre', models.CharField(blank=True, max_length=50)),
                ('segundo_nombre', models.CharField(blank=True, max_length=50)),
                ('nombre', models.CharField(blank=True, max_length=300)),
                ('fecha_nacimiento', models.DateTimeField()),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('tipo_identificacion_id', models.ForeignKey(on_delete=False, related_name='tipo_identificacion_id', to='maestros.TipoIdentificacion')),
            ],
            options={
                'verbose_name': 'Persona',
                'verbose_name_plural': 'Personas',
            },
        ),
        migrations.CreateModel(
            name='Finca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('area_total', models.CharField(blank=True, max_length=20)),
                ('area_cultivada', models.CharField(blank=True, max_length=50)),
                ('cajas_por_semana', models.CharField(blank=True, max_length=50)),
                ('codigo_magap', models.CharField(blank=True, max_length=50)),
                ('activo', models.BooleanField(default=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('sector_id', models.ForeignKey(on_delete=False, related_name='sector_id', to='maestros.Sector')),
                ('tipo_finca_id', models.ForeignKey(on_delete=False, related_name='tipo_finca_id', to='maestros.TipoFinca')),
            ],
            options={
                'verbose_name': 'Finca',
                'verbose_name_plural': 'Fincas',
            },
        ),
    ]
