from django.conf.urls import url

from .views import *

app_name='maestros'

urlpatterns = [
    url(r'^registro/$', Register.as_view()),
    url(r'^holamundo/$', HolaMundo.as_view()),
    url(r'^iniciar-sesion/$', InicioSesion.as_view()),
    url(r'^registro-finca/$', RegistrarFinca.as_view()),
    url(r'^lista-finca-general/$', FincaGeneralList.as_view()),
    url(r'^lista-finca-filtro-general/$', FincaFiltroList.as_view()),
    url(r'^lista-tipo-finca/$', TipoFincaList.as_view()),
    url(r'^lista-sector/$', SectorList.as_view()),
    url(r'^lista-tipo-caja/$', TipoCajaList.as_view()),
    url(r'^lista-notificaciones/$', ListaNotificaciones.as_view()),
    url(r'^lista-finca/$', FincaList.as_view()),
    url(r'^validar-usuario/$', ValidarUsuarioApp.as_view()),
    url(r'^lista-solicitudes/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/todos=(?P<todos>\w+)/$', PersonaListPaging.as_view()),
    url(r'^aprobar-usuario/$', AprobarUsuario.as_view()),
    url(r'^registrar-token/$', NotificacionToken.as_view()),
    url(r'^enviar-notificacion/$', EnviarNotificacion.as_view()),
    url(r'^lista-vapor/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/vapor=(?P<vapor>\w+)/todos=(?P<todos>\w+)/$', VaporListPaging.as_view()),
    url(r'^registro-vapor/$', RegistrarVapor.as_view()),
    url(r'^eliminar-vapor/$', DeleteVapor.as_view()),
    url(r'^lista-puerto/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/puerto=(?P<puerto>\w+)/todos=(?P<todos>\w+)/$', PuertoListPaging.as_view()),
    url(r'^registro-puerto/$', RegistrarPuerto.as_view()),
    url(r'^eliminar-puerto/$', DeletePuerto.as_view()),
    url(r'^iniciar-sesion-web/$', InicioSesionWeb.as_view()),
    url(r'^validar-usuario-web/$', ValidarUsuarioWeb.as_view()),
    url(r'^registrar-token-web/$', NotificacionTokenWeb.as_view()),
    url(r'^reporte_personas_pdf/$',ReportePersonasPDF.as_view()),
    url(r'^subir_archivo/$',SubirArchivo.as_view()),
    url(r'^get-persona/$',DataUsuario.as_view()),
]