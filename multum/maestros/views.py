from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from django.http import JsonResponse

import os
import json
from django.db.models import Subquery
from django.db.models.functions import *

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate, login

from fcm_django.models import FCMDevice,Device,FCMDevice
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser

from .serializers import *
from .constantes import *
from .models import *
from registro.models import PersonaRol
from registro.serializers import PersonaRolSerializer
from django.contrib.auth.models import User
from simplejson import JSONDecodeError
from multum.utils import ErrorServicio,error_servicio
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser
#Importaciones para generar el pdf
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.platypus import Table,TableStyle
from reportlab.lib import colors

from .permisos import PruebaPermission
#Apis para movil
#Manejo de inicio y registro de usuarios
class Register(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        uid = body['usuario']
        contasena = body['contrasena']
        primer_apellido = body['primer_apellido']
        segundo_apellido = body['segundo_apellido']
        primer_nombre = body['primer_nombre']
        segundo_nombre = body['segundo_nombre']
        fecha_nacimiento = body['fecha_nacimiento']
        identificacion= body['identificacion']
        telefono= body['telefono']
        direccion= body['direccion']
        correo= body['correo']
        #Logica de servicio

        tipo_identificacion_id= TipoIdentificacion.objects.all().filter(
            codigo=RUC
        ).first()
        #Validacion si usuario ya existe
        usuario=User.objects.all().filter(username=uid).first()
        if usuario:
            return Response(
              {'mensaje': 'Usuario existente',
              'status': 'failed'},)
        else:
            user = User(email=correo, username=uid)
            user.set_password(contasena)
            user.save()
            datos={
                'tipo_identificacion_id':tipo_identificacion_id.id,
                'user':user.id,
                'primer_apellido':primer_apellido,
                'identificacion':identificacion,
                'segundo_apellido':segundo_apellido,
                'primer_nombre':primer_nombre,
                'segundo_nombre':segundo_nombre,
                'nombre':primer_nombre+" "+segundo_nombre+" "+primer_apellido+" "+segundo_apellido,
                'fecha_nacimiento':fecha_nacimiento,
                'telefono':telefono,
                'direccion':direccion
            }
            serializer = PersonaSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                persona=Persona.objects.all().filter(
                    user__id=user.id
                ).first()
                rol=TipoRol.objects.all().filter(
                    codigo=PRODUCTOR
                ).first()
                agro=AgroArtesanal.objects.all().first()
                datos={
                    'persona_id': persona.id,
                    'tipo_rol_id': rol.id,
                    'agro_artesanal_id': agro.id,
                    'aprobado': False
                }
                serializer = PersonaRolSerializer(data=datos)
                print(serializer.is_valid())
                print(serializer.errors)
                if serializer.is_valid():
                    serializer.save()

                    devices = FCMDevice.objects.all().filter(
                            type="web"
                        )
                    for device in devices:
                        if(device):
                            device.send_message(
                                title="Nuevo registro productor", 
                                body="Apruebe el usuario enviado",
                                data={"landing_page": "Data devuelta de la notificacion"})
                    return Response(
                    {'mensaje': 'Usuario guardado satisfactoriamente',
                    'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)

class InicioSesion(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos Validar sea productor antes
        body = json.loads(body_unicode)
        uid = body['usuario']
        contasena = body['contasena']
        #Logica de negocio
        user = authenticate(request, username=uid, password=contasena)
        if user:
            usuario=User.objects.all().filter(username=uid).first()
            rol_usuario=PersonaRol.objects.all().filter(
                persona_id__user__id=usuario.id,
                tipo_rol_id__codigo=PRODUCTOR,
            ).first()
            if rol_usuario:
                if rol_usuario.aprobado:
                    login(request, user)
                    token, created = Token.objects.get_or_create(user=user)
                    datos = {
                        'user':usuario.username,
                        'session':request.session.session_key,
                        'token': str(token),
                        'status': 'success',
                        'mensaje':'Logeado correctamente',
                        'nombres':rol_usuario.persona_id.primer_nombre+" "+rol_usuario.persona_id.primer_apellido
                    }
                    return Response(datos)
                else:
                    return Response(
                    {'mensaje': 'El usuario no ha sido aprobado por la exportadora.',
                    'status': 'failed'},)
        return Response(
              {'mensaje': 'Datos incorrectos',
              'status': 'failed'},)

class NotificacionToken(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        token = body['token']
        dispositivo_tipo=body['dispositivo']
        print("-----------*********TOKEN***********-------------")
        dispositivo = FCMDevice.objects.all().filter(
                    user=request.user.id
        )
        if(dispositivo):
            print("-----------*********ACTUALIZACION***********-------------")
            print(request.user.id)
            device_tmp = FCMDevice.objects.all().filter(
                    user=request.user.id
            ).first()
            print(device_tmp)
            device = FCMDevice.objects.all().filter(
                    user=request.user.id
            ).update(registration_id=token)
        else:
            print("-----------*********GUARDADO***********-------------")
            device = FCMDevice()
            device.device_id = request.user.id+10
            device.registration_id = token
            device.type = "Android"
            device.name = "devide"+str(request.user.id)
            device.user = request.user
            print(device)
            device.save()
        return Response(
                    {'mensaje': 'Token registrado',
                    'status': 'success'},)

class ValidarUsuarioApp(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self,request):
        persona_rol=PersonaRol.objects.all().filter(
            tipo_rol_id__codigo=PRODUCTOR,
            persona_id__user__id=request.user.id
        ).first()
        if persona_rol:
            return Response(
              {'mensaje': 'Usuario registrado',
              'status': 'success'},)
        else:
            return Response(
              {'mensaje': 'Usuario no autorizado',
              'status': 'failed'},)

class DataUsuario(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        persona=Persona.objects.filter(user__id=request.user.id)
        serializer=PersonaSerializer(persona,many=False)
        return JsonResponse(serializer.data, safe = False)

class ListaNotificaciones(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = Notificacion.objects.all().filter(
            productor_id__user__id=request.user.id
        )
        return Response({'status':'success','length':queryset.count()})

#Registro de fincas
class RegistrarFinca(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        area_total=body['area_total']
        area_cultivada=body['area_cultivada']
        cajas_por_semana=body['cajas_por_semana']
        codigo_magap=body['codigo_magap']
        tipo_finca_id=body['tipo_finca_id']
        sector_id=body['sector_id']
        nombre=body['nombre']
        id=body['id']
        #Logica de negocio
        persona=Persona.objects.all().filter(
                user=request.user.id
        ).first()
        datos={
            'area_total':area_total,
            'area_cultivada':area_cultivada,
            'cajas_por_semana':cajas_por_semana,
            'codigo_magap':codigo_magap,
            'tipo_finca_id':tipo_finca_id,
            'sector_id':sector_id,
            'productor_id':persona.id,
            'nombre':nombre
        }
        if id==0:
            serializer = FincaSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return error_servicio()
        else:
            finca=Finca.objects.filter(id=id).first()
            serializer = FincaSerializer(finca,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return error_servicio()
            """ try:
                
            except:
                return error_servicio() """

class FincaList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = Finca.objects.all().filter(
            productor_id__user__id=request.user.id
        )
        serializer = FincaListSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)
        """ try:
            
        except:
            return error_servicio() """

#Listas
class TipoFincaList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = TipoFinca.objects.all().order_by('-nombre')
        serializer = TipoFincaListSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)
        """ try:
            
        except:
            return error_servicio() """

class TipoCajaList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = TipoCaja.objects.all().order_by('-nombre')
        serializer = TipoCajaListSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class SectorList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = Sector.objects.all().order_by('-nombre')
        serializer = SectorListSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)
        """ try:
            
        except:
            return error_servicio() """

class HolaMundo(APIView):
    permission_classes = [PruebaPermission]
    def get(self,request):
        return Response(
              {'error': 'Usuario existente',
              'status': 'failed'},)

class FincaGeneralList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = Finca.objects.all().filter(
            productor_id__user__id=request.user.id
        ).order_by('-nombre')
        serializer = FincaListMovilSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class FincaFiltroList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def post(self, request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        fecha_desde=body['fecha_desde']
        fecha_hasta=body['fecha_hasta']
        tipo=body['tipo']
        queryset = Finca.objects.all().filter(
            productor_id__user__id=request.user.id,
            created_date__gte=fecha_desde,
            created_date__lte=fecha_hasta,
            tipo_finca_id=tipo
        ).order_by('-nombre')
        serializer = FincaListMovilSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)
#Apis web
class PersonaListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        roles=PersonaRol.objects.all().filter(
            tipo_rol_id__codigo=PRODUCTOR
        )
        if todos=="true":
            queryset = Persona.objects.all().filter(
                id__in=Subquery(roles.values('persona_id'))
            )
        else:
            queryset = Persona.objects.all().filter(
                id__in=Subquery(roles.values('persona_id')))
        serializer = PersonaListSerializer(queryset.order_by('-created_date'), many = True)
        datos={
            'data':serializer.data,
            'length':Persona.objects.all().filter(id__in=Subquery(roles.values('persona_id'))).count()
        }
        return JsonResponse(datos, safe = False)

class AprobarUsuario(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        persona_id = body['persona_id']
        aprobado=body['aprobado']
        persona_rol=PersonaRol.objects.all().filter(
            persona_id=persona_id
        ).first()
        variable=True
        if aprobado=="False":
            variable=False
        datos={
            'persona_id': persona_rol.persona_id.id,
            'tipo_rol_id': persona_rol.tipo_rol_id.id,
            'agro_artesanal_id': persona_rol.agro_artesanal_id.id,
            'aprobado': aprobado
        }
        serializer = PersonaRolSerializer(persona_rol,datos)
        print(serializer.is_valid())
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(
            {'mensaje': 'Usuario aprobado',
                    'status': 'success'},)
        else:
            return Response(
            {'mensaje': 'Ocurrio un error en el servicio',
            'status': 'failed'},)

class EnviarNotificacion(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        id=body['id']
        device = FCMDevice.objects.all().filter(
                user=id
            ).last()
        if(device):
            device.send_message(
                title="Notificacion primera", 
                body="Cuerpo de la notificacion",
                data={"landing_page": "Data devuelta de la notificacion"})
            return Response(
                {'mensaje': 'Enviado correctamente',
                'status': 'success'},)
        return Response(
                {'mensaje': 'Ha ocurrido un error en el servicio',
                'status': 'failed'},)

class InicioSesionWeb(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        uid = body['usuario']
        contasena = body['contasena']
        #Logica de negocio
        user = authenticate(request, username=uid, password=contasena)
        if user:
            usuario=User.objects.all().filter(username=uid).first()
            rol_usuario=PersonaRol.objects.all().filter(
                persona_id__user__id=usuario.id,
                tipo_rol_id__codigo=EXPORTADOR
            ).first()
            if rol_usuario.aprobado:
                login(request, user)
                token, created = Token.objects.get_or_create(user=user)
                datos = {
                    'user':usuario.username,
                    'session':request.session.session_key,
                    'token': str(token),
                    'status': 'success',
                    'mensaje':'Logeado correctamente'
                }
                return Response(datos)
            else:
                return Response(
                {'mensaje': 'El usuario no ha sido aprobado por la exportadora.',
                'status': 'failed'},)
        return Response(
              {'mensaje': 'Datos incorrectos',
              'status': 'failed'},)

class NotificacionTokenWeb(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        token = body['token']
        dispositivo = FCMDevice.objects.all().filter(
                    user=request.user.id
            )
        if(dispositivo):
            device = FCMDevice.objects.all().filter(
                    user=request.user.id
            ).update(registration_id=token)
        else:
            device = FCMDevice()
            device.device_id = request.user.id+10
            device.registration_id = token
            device.type = "web"
            device.name = "devide"+str(request.user.id)
            device.user = request.user
            device.save()
        return Response(
                    {'mensaje': 'Token registrado',
                    'status': 'success'},)

#Mantenimientos
#Vapor
class VaporListPaging(APIView):
    def get(self, request,pagina,elementos,vapor,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if vapor=="null":
            if todos=="true":
                queryset = Vapor.objects.all().order_by('-created_date')
                serializer = VaporListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Vapor.objects.all().count()
                }
                
                return JsonResponse(datos, safe = False)
            else:
                queryset = Vapor.objects.all().order_by('-created_date')[inicio:fin]
                serializer = VaporListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Vapor.objects.all().count()
                }
                
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = Vapor.objects.all().filter(
                    nombre__contains=vapor).order_by('-created_date')
                serializer = VaporListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Vapor.objects.all().filter(nombre=vapor).count()
                }
                
                return JsonResponse(datos, safe = False)
            else:
                queryset = Vapor.objects.all().filter(
                    nombre__contains=vapor
                ).order_by('-created_date')[inicio:fin]
                serializer = VaporListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Vapor.objects.all().filter(nombre=vapor).count()
                }
                
                return JsonResponse(datos, safe = False)

class RegistrarVapor(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        id=body['id']
        vapor=body['vapor']
        #Logica de negocio
        datos={
            'nombre':vapor
        }
        if id==0:
            serializer = VaporSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                        {'mensaje': 'Vapor guardado satisfactoriamente',
                        'status': 'success'},)
            else:
                return Response(
                    {'mensaje': 'Ha ocurrido un error en el servicio',
                    'status': 'failed'},)
        else:
            vapor_obj=Vapor.objects.all().filter(
                id=id
            ).first()
            serializer = VaporSerializer(vapor_obj,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                        {'mensaje': 'Vapor guardado satisfactoriamente',
                        'status': 'success'},)
            else:
                return Response(
                    {'mensaje': 'Ha ocurrido un error en el servicio',
                    'status': 'failed'},)

class DeleteVapor(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        id=body['id']
        vapor=Vapor.objects.all().filter(
            id=id
        ).first()
        if vapor:
            vapor.delete()
            return Response(
                        {'mensaje': 'Vapor eliminado satisfactoriamente',
                        'status': 'success'},)
        else:
            return Response(
                {'mensaje': 'Ha ocurrido un error en el servicio',
                'status': 'failed'},)

#Puerto
class PuertoListPaging(APIView):
    def get(self, request,pagina,elementos,puerto,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        
        
        if puerto=="null":
            if todos=="true":
                queryset = Puerto.objects.all().order_by('-created_date')
                serializer = PuertoListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Puerto.objects.all().count()
                }
                
                return JsonResponse(datos, safe = False)
            else:
                queryset = Puerto.objects.all().order_by('-created_date')[inicio:fin]
                serializer = PuertoListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Puerto.objects.all().count()
                }
                
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = Puerto.objects.all().filter(
                    nombre__contains=puerto).order_by('-created_date')
                serializer = PuertoListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Puerto.objects.all().filter(nombre=puerto).count()
                }
                
                return JsonResponse(datos, safe = False)
            else:
                queryset = Puerto.objects.all().filter(
                    nombre__contains=puerto
                ).order_by('-created_date')[inicio:fin]
                serializer = PuertoListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':Puerto.objects.all().filter(nombre=puerto).count()
                }
                
                return JsonResponse(datos, safe = False)

class RegistrarPuerto(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        id=body['id']
        puerto=body['puerto']
        #Logica de negocio
        datos={
            'nombre':puerto
        }
        if id==0:
            serializer = PuertoSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                        {'mensaje': 'Puerto guardado satisfactoriamente',
                        'status': 'success'},)
            else:
                return Response(
                    {'mensaje': 'Ha ocurrido un error en el servicio',
                    'status': 'failed'},)
        else:
            puerto_obj=Puerto.objects.all().filter(
                id=id
            ).first()
            serializer = PuertoSerializer(puerto_obj,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                        {'mensaje': 'Puerto guardado satisfactoriamente',
                        'status': 'success'},)
            else:
                return Response(
                    {'mensaje': 'Ha ocurrido un error en el servicio',
                    'status': 'failed'},)

class DeletePuerto(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        #Data recibida y parseo de datos
        body = json.loads(body_unicode)
        id=body['id']
        puerto=Puerto.objects.all().filter(
            id=id
        ).first()
        if puerto:
            puerto.delete()
            return Response(
                        {'mensaje': 'Puerto eliminado satisfactoriamente',
                        'status': 'success'},)
        else:
            return Response(
                {'mensaje': 'Ha ocurrido un error en el servicio',
                'status': 'failed'},)

class ValidarUsuarioWeb(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self,request):
        persona_rol=PersonaRol.objects.all().filter(
            tipo_rol_id__codigo=EXPORTADOR,
            persona_id__user__id=request.user.id
        ).first()
        if persona_rol:
            return Response(
              {'mensaje': 'Usuario registrado',
              'status': 'success'},)
        else:
            return Response(
              {'mensaje': 'Usuario no autorizado',
              'status': 'failed'},)

#Generar pdfs
class ReportePersonasPDF(APIView):
    def cabecera(self,pdf):
        #Utilizamos el archivo logo_django.png que está guardado en la carpeta media/imagenes
        #archivo_imagen = settings.MEDIA_ROOT+'/imagenes/logo_django.png'
        #Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
        #pdf.drawImage(archivo_imagen, 40, 750, 120, 90,preserveAspectRatio=True)
        #Establecemos el tamaño de letra en 16 y el tipo de letra Helvetica
        pdf.setFont("Helvetica", 16)
        #Dibujamos una cadena en la ubicación X,Y especificada
        pdf.drawString(230, 790, u"MULTUM")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"REPORTE DE VAPOR")

    def get(self, request, *args, **kwargs):
        try:
            #Indicamos el tipo de contenido a devolver, en este caso un pdf
            response = HttpResponse(content_type='application/pdf')
            #La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
            buffer = BytesIO()
            #Canvas nos permite hacer el reporte con coordenadas X y Y
            pdf = canvas.Canvas(buffer)
            #Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
            self.cabecera(pdf)
            y = 600
            self.tabla(pdf, y)
            #Con show page hacemos un corte de página para pasar a la siguiente
            pdf.showPage()
            pdf.save()
            pdf = buffer.getvalue()
            buffer.close()
            response.write(pdf)
            return response
        except Exception as identifier:
            print(identifier)
            return Response(
              {'mensaje': str(identifier),
              'status': 'failed'},)
            
        

    def tabla(self,pdf,y):
        #Creamos una tupla de encabezados para neustra tabla
        encabezados = ('Id','Nombre', 'Activo')
        #Creamos una lista de tuplas que van a contener a las personas
        detalles = [(vapor.id*100,vapor.nombre, vapor.activo) for vapor in Vapor.objects.all()]
        #Establecemos el tamaño de cada una de las columnas de la tabla
        cm=10
        detalle_orden = Table([encabezados] + detalles, colWidths=[30 , 7 * cm, 5 * cm])
        #Aplicamos estilos a las celdas de la tabla
        detalle_orden.setStyle(TableStyle(
        [
                #La primera fila(encabezados) va a estar centrada
                ('ALIGN',(0,0),(2,0),'CENTER'),
                #Los bordes de todas las celdas serán de color negro y con un grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                #El tamaño de las letras de cada una de las celdas será de 10
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ]
        ))
        #Establecemos el tamaño de la hoja que ocupará la tabla
        detalle_orden.wrapOn(pdf, 800, 600)
        #Definimos la coordenada donde se dibujará la tabla
        detalle_orden.drawOn(pdf, 60,y)

class SubirArchivo(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    parser_class = (MultiPartParser,)

    def post(self,request,*args, **kwargs):
        archivo=request.data['file']
        for data in archivo:
            tmp=data.decode()
            datos=tmp[:-2].split(";")
            
        return Response(
              {'mensaje': 'Usuario registrado',
              'status': 'success'},)