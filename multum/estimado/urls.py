from django.conf.urls import url

from .views import *

app_name='estimado'

urlpatterns = [
    url(r'^crear-estimado/$', RegistrarEstimadoCaja.as_view()),
    url(r'^lista-estimados/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', EstimadoListPaging.as_view()),
    url(r'^lista-seleccion-estimados/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', EstimadoSeleccionListPaging.as_view()),
    url(r'^lista-movil-estimados/finca=(?P<finca>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', MEstimadoList.as_view()),
]