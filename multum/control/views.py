from rest_framework.parsers import JSONParser, FormParser
from rest_framework import generics, status, viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework_jwt.settings import api_settings
from rest_framework.authtoken.models import Token
#import jwt
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404, render, redirect
from django.http import Http404, JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .serializers import RacimoControlSerializer, CajaControlSerializer
from .models import CajaControl, RacimoControl, ControlColor, ControlEdad
from maestros.models import TipoCaja,Finca
from envio.models import Embarque, OrdenCorte
from estimado.models import EstimadoCaja
from estimado.serializers import EstimadoCajaSerializer
from envio.serializers import OrdenCorteGuardadoSerializer
from enfunde.models import Enfunde, Cinta
import random as rd
import os
import time
import json
from django.db.models import Subquery, F, Func
from django.db.models.functions import *
import math

from datetime import datetime, date
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import Q

from fcm_django.models import FCMDevice
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from django.db.models import Sum


class RegistroRacimoControl(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        # Data recibida y parseo de datos
        listado = body['listado']
        orden_corte_id = body['orden_corte_id']
        rechazo = body['total_racimos_rechazado']
        orden = OrdenCorte.objects.filter(id=orden_corte_id).first()
        cinta_id = Cinta.objects.filter(
            semana=orden.semana, anio=orden.anio).first()
        control_color = ControlColor.objects.filter(
            color=cinta_id.color).first()
        lista_colores = ControlColor.objects.all().order_by('orden_edad')
        control_color_edad = ControlEdad.objects.first()
        edades = {control_color.color: control_color_edad.edad_referencia}
        control_color_id = control_color.orden_edad
        for i in range(control_color_edad.edad_referencia-1, control_color_edad.edad_minima, -1):
            control_color_id = control_color_id-1
            if(control_color_id <= 0):
                control_color_id = lista_colores.last().orden_edad
                edades[lista_colores.last().color] = i
            else:
                tmp_control = ControlColor.objects.filter(
                    orden_edad=control_color_id).first()
                edades[tmp_control.color] = i
        control_color_id = control_color.orden_edad
        for i in range(control_color_edad.edad_referencia+1, control_color_edad.edad_maxima+1):
            control_color_id = control_color_id+1
            if(control_color_id > 6):
                control_color_id = 1
                edades[lista_colores.first().color] = i
            else:
                tmp_control = ControlColor.objects.filter(
                    orden_edad=control_color_id).first()
                edades[tmp_control.color] = i
        datos = {
            'orden_corte_id': orden_corte_id,
            'controlcolor_id': None,
            'edad': 0,
            'rechazo': True,
            'cantidad': rechazo
        }
        serializer = RacimoControlSerializer(data=datos)
        print(serializer.is_valid())
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()

        for elemento in listado:
            controlcolor_id = ControlColor.objects.filter(
                color=elemento['color']).first()
            datos = {
                'orden_corte_id': orden_corte_id,
                'controlcolor_id': controlcolor_id.id,
                'edad': edades[elemento['color']],
                'rechazo': False,
                'cantidad': elemento['cantidad']
            }
            # id','orden_corte_id','controlcolor_id','edad','rechazo','cantidad
            # print(datos)
            serializer = RacimoControlSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(
                    {'mensaje': 'Ocurrio un error en el servicio',
                     'status': 'failed'},)

        datos = {
            'racimo_terminado': True,
            'anio': orden.anio,
            'semana': orden.semana,
            'cantidad': orden.cantidad,
            'finca_id': orden.finca_id.id,
            'embarque_id': orden.embarque_id.id,
        }
        serializer = OrdenCorteGuardadoSerializer(orden, data=datos)
        print(serializer.is_valid())
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {'mensaje': 'Guardado exitosamente',
                 'status': 'success'},)
        else:
            return Response(
                {'mensaje': 'Error en el servicio',
                 'status': 'failed'},)


""" select id,cajas_terminado from envio_ordencorte where id =19
update envio_ordencorte	set cajas_terminado = false where id=19
select * from control_controledad
select * from control_controlcolor order by orden_edad
select * from enfunde_enfunde where semana=38 and anio=2019
select * from enfunde_cinta 
select * from envio_ordencorte 
SELECT (CASE WHEN (control_controlcolor.orden_edad + controlcolor.posicion) > 
		(SELECT control_controledad.edad_maxima FROM control_controledad) 
		THEN ((control_controlcolor.orden_edad + controlcolor.posicion) - 
			  (SELECT control_controledad.edad_maxima FROM control_controledad) + 
			  (SELECT control_controledad.edad_minima FROM control_controledad)) 
		ELSE (control_controlcolor.orden_edad + controlcolor.posicion) END) 
		AS edadsem, control_controlcolor.color, enfunde_cinta.anio, 
		enfunde_cinta.semana FROM control_controlcolor, enfunde_cinta, 
		(SELECT ((SELECT control_controledad.edad_referencia FROM control_controledad)
				 - control_controlcolor.orden_edad) AS posicion FROM enfunde_cinta, 
		 control_controlcolor WHERE enfunde_cinta.anio=2020 
		 AND enfunde_cinta.semana=1 
		 AND control_controlcolor.color = enfunde_cinta.color ) controlcolor 
		 WHERE enfunde_cinta.anio=2020 AND enfunde_cinta.semana=1 ORDER BY 1;
"""


class RegistroCajaControl(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        # Data recibida y parseo de datos
        listado = body['listado_cajas']
        orden_corte_id = body['orden_corte_id']
        segunda = body['segunda']
        for key, value in segunda.items():
            if(value != 0):
                tipo_caja = TipoCaja.objects.filter(peso=key).first()
                tipo_caja_id = tipo_caja.id
                cantidad = value
                datos = {
                    'orden_corte_id': orden_corte_id,
                    'tipo_caja_id': tipo_caja_id,
                    'segunda': False,
                    'cantidad': cantidad
                }
                """ serializer = CajaControlSerializer(data=datos)
                print(serializer.is_valid())
                print(serializer.errors)
                if serializer.is_valid():
                    serializer.save() """
        for key, value in listado.items():
            if(value != 0):
                tipo_caja = TipoCaja.objects.filter(peso=key).first()
                tipo_caja_id = tipo_caja.id
                cantidad = value
                datos = {
                    'orden_corte_id': orden_corte_id,
                    'tipo_caja_id': tipo_caja_id,
                    'segunda': False,
                    'cantidad': cantidad
                }
                serializer = CajaControlSerializer(data=datos)
                print(serializer.is_valid())
                print(serializer.errors)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(
                        {'mensaje': 'Ocurrio un error en el servicio',
                         'status': 'failed'},)
        orden = OrdenCorte.objects.filter(id=orden_corte_id).first()
        datos = {
            'cajas_terminado': True,
            'anio': orden.anio,
            'semana': orden.semana,
            'cantidad': orden.cantidad,
            'finca_id': orden.finca_id.id,
            'embarque_id': orden.embarque_id.id,
        }
        serializer = OrdenCorteGuardadoSerializer(orden, data=datos)
        print(serializer.is_valid())
        print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {'mensaje': 'Guardado exitosamente',
                 'status': 'success'},)
        else:
            return Response(
                {'mensaje': 'Error en el servicio',
                 'status': 'failed'},)


class ProduccionDatos(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 

    def get(self, request, anio, sem_desde, sem_hasta,finca,todos):
        # Data recibida y parseo de datos
        resultado_finca=[]
        resultado = []
        tmp_contador = 0
        tmp_inventario_final = 0  # para calculo de recobro
        if todos=="true":
            fincas=Finca.objects.filter(productor_id__user__id=request.user.id)
            for finca in fincas:
                ordenes_corte = OrdenCorte.objects.all().filter(
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta,
                finca_id__id=finca.id).order_by('anio', 'semana')
                resultado = []
                tmp_contador = 0
                tmp_inventario_final = 0
                for orden in ordenes_corte:
                    datos = {
                        'anio': orden.anio,
                        'semana': orden.semana,
                        'finca': orden.finca_id.nombre,
                        'orden': orden.id
                    }
                    # Calculo de cajas: total, segunda y sus valores
                    caja_controles = CajaControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        segunda=False
                    )
                    suma = 0
                    data_cajas = {}
                    for caja in caja_controles:
                        suma += caja.cantidad*caja.tipo_caja_id.peso
                        data_cajas[str(caja.tipo_caja_id.peso)] = caja.cantidad
                    total_cajas = suma/43
                    caja_controles_segunda = CajaControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        segunda=True
                    )
                    suma = 0
                    for caja in caja_controles_segunda:
                        suma += caja.cantidad
                    total_cajas_segunda = 0
                    if(((suma*50)+total_cajas*43) != 0):
                        total_cajas_segunda = (suma*50)/((suma*50)+total_cajas*43)*100
                    datos['cajas'] = {
                        'total_cajas': round(total_cajas, 0),
                        'porcentaje_segunda': round(total_cajas_segunda, 2),
                        'listado_cajas': data_cajas,
                        'caja_segunda': {'50': suma}
                    }
                    # Racimos
                    total_racimos = RacimoControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        rechazo=False
                    ).aggregate(Sum('cantidad'))
                    data_racimos = []
                    for racimo in RacimoControl.objects.all().filter(orden_corte_id=orden.id, rechazo=False):
                        tmp_datos_racimo = {
                            'color': racimo.controlcolor_id.color,
                            'cantidad': racimo.cantidad,
                            'edad': racimo.edad
                        }
                        data_racimos.append(tmp_datos_racimo)
                    total_racimos_rechazado = RacimoControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        rechazo=True
                    ).aggregate(Sum('cantidad'))

                    if(total_racimos['cantidad__sum']):
                        ratio = total_cajas/total_racimos['cantidad__sum']
                    else:
                        ratio = 0
                    datos['racimos'] = {
                        'total_racimos': total_racimos['cantidad__sum'],
                        'total_racimos_rechazado': total_racimos_rechazado['cantidad__sum'],
                        'ratio': round(ratio, 2),
                        'data_racimos': data_racimos
                    }
                    # Inventarios
                    inventario = {}
                    datos_inventarios = []
                    total_inventario_inicial = 0
                    total_inventario_final = 0
                    for i in range(8, 15):
                        semana = 0
                        bandera = True
                        if((orden.semana-i) > 0):
                            semana = orden.semana-i
                            enfunde = Enfunde.objects.all().filter(
                                semana=semana,
                                anio=orden.anio,
                                finca_id=orden.finca_id.id).first()
                        else:
                            semana = 52-(i-orden.semana)
                            bandera = False
                            """ if(tmp_contador==15):
                                print(semana) """
                            enfunde = Enfunde.objects.all().filter(
                                semana=semana,
                                anio=orden.anio-1,
                                finca_id=orden.finca_id.id
                            ).first()
                        tmp_enfunde_cantidad = enfunde.cantidad if(enfunde) else 0
                        total = tmp_enfunde_cantidad
                        total_final = tmp_enfunde_cantidad
                        racimo_gen = RacimoControl.objects.all().filter(
                            orden_corte_id__semana=orden.semana,
                            orden_corte_id__anio=orden.anio,
                            edad=i,
                            rechazo=False).first()
                        total_final = total_final - \
                            racimo_gen.cantidad if(racimo_gen) else total_final
                        contador = 0
                        contador_semana = 1
                        if(bandera):
                            for j in reversed(range(8, i)):
                                racimo = RacimoControl.objects.all().filter(
                                    orden_corte_id__semana=orden.semana-contador,
                                    orden_corte_id__anio=orden.anio,
                                    edad=j,
                                    rechazo=False).first()
                                total = total-racimo.cantidad if(racimo) else total
                                total_final = total_final - \
                                    racimo.cantidad if(racimo) else total_final
                        else:
                            for j in reversed(range(8, i)):
                                if(orden.semana-contador_semana > 0):
                                    """ if(tmp_contador==16):
                                        print("normal") """
                                    racimo = RacimoControl.objects.all().filter(
                                        orden_corte_id__semana=orden.semana-contador_semana,
                                        orden_corte_id__anio=orden.anio,
                                        edad=j,
                                        rechazo=False).first()
                                    if racimo:
                                        total = total-racimo.cantidad
                                        total_final = total_final-racimo.cantidad
                                        """ if(tmp_contador==17):
                                            print(str(racimo.cantidad)+"-"+str(orden.semana-contador_semana)+"-"+str(orden.anio)+"-"+str(j)) """
                                else:
                                    tmp_semana = 52-(contador_semana-orden.semana)
                                    """ if(tmp_contador==16):
                                        print("no normal: "+str(tmp_semana)) """
                                    racimo = RacimoControl.objects.all().filter(
                                        orden_corte_id__semana=tmp_semana,
                                        orden_corte_id__anio=orden.anio-1,
                                        edad=j,
                                        rechazo=False).first()
                                    if racimo:
                                        total = total-racimo.cantidad
                                        total_final = total_final-racimo.cantidad
                                        """ if(tmp_contador==17):
                                            print(str(racimo.cantidad)+"-"+str(tmp_semana)+"-"+str(orden.anio-1)+"-"+str(j)) """
                                contador_semana = contador_semana+1
                                contador = contador+1
                        """ if(tmp_contador==17):
                            print("Resultado: "+str(orden.anio)+"-"+str(orden.semana)+"-"+str(total)+"-"+str(total_final)) """
                        total_inventario_inicial += total
                        total_inventario_final += total_final
                        """ datos_inventario_inicial[str(i)]=total
                        datos_inventario_final[str(i)]=total_final """
                        if(i == 14):
                            tmp_inventario_final = total_final
                        tmp_data_inventario = {
                            'cinta': racimo_gen.controlcolor_id.color if(racimo_gen) else "",
                            'edad': i,
                            'inicial': total,
                            'final': total_final
                        }
                        datos_inventarios.append(tmp_data_inventario)
                    inventario['inventarios'] = datos_inventarios
                    inventario['total_inicial'] = total_inventario_inicial
                    inventario['total_final'] = total_inventario_final
                    datos['inventario'] = inventario
                    """ datos['inventario_inicial']=datos_inventario_inicial
                    datos['inventario_final']=datos_inventario_final """
                    semana = 0
                    anio = 0
                    if(orden.semana-14 > 0):
                        semana = orden.semana-14
                        anio = orden.anio
                    else:
                        anio = orden.anio-1
                        semana = 52-(14-orden.semana)
                    if(tmp_contador != 0):
                        enfunde = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                        datos['racimos']['recobro'] = round(
                            (1-(tmp_inventario_final/enfunde.cantidad))*100) if(enfunde) else 0

                    # Tendencia
                    tendencia = {}
                    tendencias = []
                    tendencia_total_racimo = 0
                    tendencia_semana = {}
                    tendencia_total_cajas = 0
                    for i in range(8, 15):
                        semana_tmp = orden.semana-1 if(orden.semana-1 > 0) else 52
                        semana = 0
                        tmp_total = 0
                        anio = 0
                        if((orden.semana-i) > 0):
                            semana = orden.semana-i
                            anio = orden.anio
                            enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                            enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=semana_tmp,
                                orden_corte_id__anio=anio,
                                edad=i,
                                rechazo=False).first()
                        else:
                            semana = 52-(i-orden.semana)
                            anio = orden.anio-1
                            enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                            enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=semana_tmp,
                                orden_corte_id__anio=orden.anio if(
                                    orden.semana-1 > 0) else orden.anio-1,
                                edad=i,
                                rechazo=False).first()
                        if(racimo and enfunde_a and enfunde_b):
                            tmp_total = (
                                (racimo.cantidad/enfunde_b.cantidad)*enfunde_a.cantidad)
                        else:
                            """ print("-----------")
                            print("Resumen: "+str(semana)+"-"+str(anio)+"-"+str(i))
                            print("Semana: "+str(orden.semana))
                            print("racimo: "+str(racimo))
                            print("enfunde_a: "+str(enfunde_a))
                            print("enfunde_b: "+str(enfunde_b)) """
                        # tendencia_semana[str(i)]=round(tmp_total,0)
                        tendencia_semana = {
                            'cinta': racimo.controlcolor_id.color if(racimo) else "",
                            'edad': i,
                            'cantidad': round(tmp_total, 0)
                        }
                        tendencias.append(tendencia_semana)
                        tendencia_total_racimo += tmp_total

                    if tmp_contador > 0:
                        tendencia_total_cajas = tendencia_total_racimo * \
                            resultado[tmp_contador-1]['racimos']['ratio']
                        datos['tendencia'] = tendencia
                    tendencia['tendencias'] = tendencias
                    tendencia['total_racimos'] = round(tendencia_total_racimo, 0)
                    tendencia['total_cajas'] = round(tendencia_total_cajas, 0)
                    tmp_contador = tmp_contador+1
                    resultado.append(datos)

                resultado_finca.append(resultado)
            return Response(
            {'mensaje': 'Consulta exitosa',
             'status': 'sucess',
             'data': resultado_finca},)
        elif(todos=="false"):
            ordenes_corte = OrdenCorte.objects.all().filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta,
            finca_id__id=finca).order_by('anio', 'semana')
            for orden in ordenes_corte:
                datos = {
                    'anio': orden.anio,
                    'semana': orden.semana,
                    'finca': orden.finca_id.nombre,
                    'orden': orden.id
                }
                # Calculo de cajas: total, segunda y sus valores
                caja_controles = CajaControl.objects.all().filter(
                    orden_corte_id=orden.id,
                    segunda=False
                )
                suma = 0
                data_cajas = {}
                for caja in caja_controles:
                    suma += caja.cantidad*caja.tipo_caja_id.peso
                    data_cajas[str(caja.tipo_caja_id.peso)] = caja.cantidad
                total_cajas = suma/43
                caja_controles_segunda = CajaControl.objects.all().filter(
                    orden_corte_id=orden.id,
                    segunda=True
                )
                suma = 0
                for caja in caja_controles_segunda:
                    suma += caja.cantidad
                total_cajas_segunda = 0
                if(((suma*50)+total_cajas*43) != 0):
                    total_cajas_segunda = (suma*50)/((suma*50)+total_cajas*43)*100
                datos['cajas'] = {
                    'total_cajas': round(total_cajas, 0),
                    'porcentaje_segunda': round(total_cajas_segunda, 2),
                    'listado_cajas': data_cajas,
                    'caja_segunda': {'50': suma}
                }
                # Racimos
                total_racimos = RacimoControl.objects.all().filter(
                    orden_corte_id=orden.id,
                    rechazo=False
                ).aggregate(Sum('cantidad'))
                data_racimos = []
                for racimo in RacimoControl.objects.all().filter(orden_corte_id=orden.id, rechazo=False):
                    tmp_datos_racimo = {
                        'color': racimo.controlcolor_id.color,
                        'cantidad': racimo.cantidad,
                        'edad': racimo.edad
                    }
                    data_racimos.append(tmp_datos_racimo)
                total_racimos_rechazado = RacimoControl.objects.all().filter(
                    orden_corte_id=orden.id,
                    rechazo=True
                ).aggregate(Sum('cantidad'))
                if(total_racimos['cantidad__sum']):
                    ratio = total_cajas/total_racimos['cantidad__sum']
                else:
                    ratio = 0
                datos['racimos'] = {
                    'total_racimos': total_racimos['cantidad__sum'],
                    'total_racimos_rechazado': total_racimos_rechazado['cantidad__sum'],
                    'ratio': round(ratio, 2),
                    'data_racimos': data_racimos
                }
                # Inventarios
                inventario = {}
                datos_inventarios = []
                total_inventario_inicial = 0
                total_inventario_final = 0
                for i in range(8, 15):
                    semana = 0
                    bandera = True
                    if((orden.semana-i) > 0):
                        semana = orden.semana-i
                        enfunde = Enfunde.objects.all().filter(
                            semana=semana,
                            anio=orden.anio,
                            finca_id=orden.finca_id.id).first()
                    else:
                        semana = 52-(i-orden.semana)
                        bandera = False
                        """ if(tmp_contador==15):
                            print(semana) """
                        enfunde = Enfunde.objects.all().filter(
                            semana=semana,
                            anio=orden.anio-1,
                            finca_id=orden.finca_id.id
                        ).first()
                    tmp_enfunde_cantidad = enfunde.cantidad if(enfunde) else 0
                    total = tmp_enfunde_cantidad
                    total_final = tmp_enfunde_cantidad
                    racimo_gen = RacimoControl.objects.all().filter(
                        orden_corte_id__semana=orden.semana,
                        orden_corte_id__anio=orden.anio,
                        edad=i,
                        rechazo=False).first()
                    total_final = total_final - \
                        racimo_gen.cantidad if(racimo_gen) else total_final
                    contador = 0
                    contador_semana = 1
                    if(bandera):
                        for j in reversed(range(8, i)):
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=orden.semana-contador,
                                orden_corte_id__anio=orden.anio,
                                edad=j,
                                rechazo=False).first()
                            total = total-racimo.cantidad if(racimo) else total
                            total_final = total_final - \
                                racimo.cantidad if(racimo) else total_final
                    else:
                        for j in reversed(range(8, i)):
                            if(orden.semana-contador_semana > 0):
                                """ if(tmp_contador==16):
                                    print("normal") """
                                racimo = RacimoControl.objects.all().filter(
                                    orden_corte_id__semana=orden.semana-contador_semana,
                                    orden_corte_id__anio=orden.anio,
                                    edad=j,
                                    rechazo=False).first()
                                if racimo:
                                    total = total-racimo.cantidad
                                    total_final = total_final-racimo.cantidad
                                    """ if(tmp_contador==17):
                                        print(str(racimo.cantidad)+"-"+str(orden.semana-contador_semana)+"-"+str(orden.anio)+"-"+str(j)) """
                            else:
                                tmp_semana = 52-(contador_semana-orden.semana)
                                """ if(tmp_contador==16):
                                    print("no normal: "+str(tmp_semana)) """
                                racimo = RacimoControl.objects.all().filter(
                                    orden_corte_id__semana=tmp_semana,
                                    orden_corte_id__anio=orden.anio-1,
                                    edad=j,
                                    rechazo=False).first()
                                if racimo:
                                    total = total-racimo.cantidad
                                    total_final = total_final-racimo.cantidad
                                    """ if(tmp_contador==17):
                                        print(str(racimo.cantidad)+"-"+str(tmp_semana)+"-"+str(orden.anio-1)+"-"+str(j)) """
                            contador_semana = contador_semana+1
                            contador = contador+1
                    """ if(tmp_contador==17):
                        print("Resultado: "+str(orden.anio)+"-"+str(orden.semana)+"-"+str(total)+"-"+str(total_final)) """
                    total_inventario_inicial += total
                    total_inventario_final += total_final
                    """ datos_inventario_inicial[str(i)]=total
                    datos_inventario_final[str(i)]=total_final """
                    if(i == 14):
                        tmp_inventario_final = total_final
                    tmp_data_inventario = {
                        'cinta': racimo_gen.controlcolor_id.color if(racimo_gen) else "",
                        'edad': i,
                        'inicial': total,
                        'final': total_final
                    }
                    datos_inventarios.append(tmp_data_inventario)
                inventario['inventarios'] = datos_inventarios
                inventario['total_inicial'] = total_inventario_inicial
                inventario['total_final'] = total_inventario_final
                datos['inventario'] = inventario
                """ datos['inventario_inicial']=datos_inventario_inicial
                datos['inventario_final']=datos_inventario_final """
                semana = 0
                anio = 0
                if(orden.semana-14 > 0):
                    semana = orden.semana-14
                    anio = orden.anio
                else:
                    anio = orden.anio-1
                    semana = 52-(14-orden.semana)
                if(tmp_contador != 0):
                    enfunde = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                    datos['racimos']['recobro'] = round(
                        (1-(tmp_inventario_final/enfunde.cantidad))*100) if(enfunde) else 0
                # Tendencia
                tendencia = {}
                tendencias = []
                tendencia_total_racimo = 0
                tendencia_semana = {}
                tendencia_total_cajas = 0
                for i in range(8, 15):
                    semana_tmp = orden.semana-1 if(orden.semana-1 > 0) else 52
                    semana = 0
                    tmp_total = 0
                    anio = 0
                    if((orden.semana-i) > 0):
                        semana = orden.semana-i
                        anio = orden.anio
                        enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                        enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                        racimo = RacimoControl.objects.all().filter(
                            orden_corte_id__semana=semana_tmp,
                            orden_corte_id__anio=anio,
                            edad=i,
                            rechazo=False).first()
                    else:
                        semana = 52-(i-orden.semana)
                        anio = orden.anio-1
                        enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                        enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                        racimo = RacimoControl.objects.all().filter(
                            orden_corte_id__semana=semana_tmp,
                            orden_corte_id__anio=orden.anio if(
                                orden.semana-1 > 0) else orden.anio-1,
                            edad=i,
                            rechazo=False).first()
                    if(racimo and enfunde_a and enfunde_b):
                        tmp_total = (
                            (racimo.cantidad/enfunde_b.cantidad)*enfunde_a.cantidad)
                    else:
                        """ print("-----------")
                        print("Resumen: "+str(semana)+"-"+str(anio)+"-"+str(i))
                        print("Semana: "+str(orden.semana))
                        print("racimo: "+str(racimo))
                        print("enfunde_a: "+str(enfunde_a))
                        print("enfunde_b: "+str(enfunde_b)) """
                    # tendencia_semana[str(i)]=round(tmp_total,0)
                    tendencia_semana = {
                        'cinta': racimo.controlcolor_id.color if(racimo) else "",
                        'edad': i,
                        'cantidad': round(tmp_total, 0)
                    }
                    tendencias.append(tendencia_semana)
                    tendencia_total_racimo += tmp_total
                if tmp_contador > 0:
                    tendencia_total_cajas = tendencia_total_racimo * \
                        resultado[tmp_contador-1]['racimos']['ratio']
                    datos['tendencia'] = tendencia
                tendencia['tendencias'] = tendencias
                tendencia['total_racimos'] = round(tendencia_total_racimo, 0)
                tendencia['total_cajas'] = round(tendencia_total_cajas, 0)
                tmp_contador = tmp_contador+1
                resultado.append(datos)
            return Response(
            {'mensaje': 'Consulta exitosa',
             'status': 'sucess',
             'data': resultado},)
        else:
            fincas=Finca.objects.filter(productor_id__user__id=request.user.id)
            for finca in fincas:
                ordenes_corte = OrdenCorte.objects.all().filter(
                finca_id__id=finca.id).order_by('anio', 'semana')
                resultado = []
                tmp_contador = 0
                tmp_inventario_final = 0
                for orden in ordenes_corte:
                    datos = {
                        'anio': orden.anio,
                        'semana': orden.semana,
                        'finca': orden.finca_id.nombre,
                        'orden': orden.id
                    }
                    # Calculo de cajas: total, segunda y sus valores
                    caja_controles = CajaControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        segunda=False
                    )
                    suma = 0
                    data_cajas = {}
                    for caja in caja_controles:
                        suma += caja.cantidad*caja.tipo_caja_id.peso
                        data_cajas[str(caja.tipo_caja_id.peso)] = caja.cantidad
                    total_cajas = suma/43
                    caja_controles_segunda = CajaControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        segunda=True
                    )
                    suma = 0
                    for caja in caja_controles_segunda:
                        suma += caja.cantidad
                    total_cajas_segunda = 0
                    if(((suma*50)+total_cajas*43) != 0):
                        total_cajas_segunda = (suma*50)/((suma*50)+total_cajas*43)*100
                    datos['cajas'] = {
                        'total_cajas': round(total_cajas, 0),
                        'porcentaje_segunda': round(total_cajas_segunda, 2),
                        'listado_cajas': data_cajas,
                        'caja_segunda': {'50': suma}
                    }
                    # Racimos
                    total_racimos = RacimoControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        rechazo=False
                    ).aggregate(Sum('cantidad'))
                    data_racimos = []
                    for racimo in RacimoControl.objects.all().filter(orden_corte_id=orden.id, rechazo=False):
                        tmp_datos_racimo = {
                            'color': racimo.controlcolor_id.color,
                            'cantidad': racimo.cantidad,
                            'edad': racimo.edad
                        }
                        data_racimos.append(tmp_datos_racimo)
                    total_racimos_rechazado = RacimoControl.objects.all().filter(
                        orden_corte_id=orden.id,
                        rechazo=True
                    ).aggregate(Sum('cantidad'))

                    if(total_racimos['cantidad__sum']):
                        ratio = total_cajas/total_racimos['cantidad__sum']
                    else:
                        ratio = 0
                    datos['racimos'] = {
                        'total_racimos': total_racimos['cantidad__sum'],
                        'total_racimos_rechazado': total_racimos_rechazado['cantidad__sum'],
                        'ratio': round(ratio, 2),
                        'data_racimos': data_racimos
                    }
                    # Inventarios
                    inventario = {}
                    datos_inventarios = []
                    total_inventario_inicial = 0
                    total_inventario_final = 0
                    for i in range(8, 15):
                        semana = 0
                        bandera = True
                        if((orden.semana-i) > 0):
                            semana = orden.semana-i
                            enfunde = Enfunde.objects.all().filter(
                                semana=semana,
                                anio=orden.anio,
                                finca_id=orden.finca_id.id).first()
                        else:
                            semana = 52-(i-orden.semana)
                            bandera = False
                            """ if(tmp_contador==15):
                                print(semana) """
                            enfunde = Enfunde.objects.all().filter(
                                semana=semana,
                                anio=orden.anio-1,
                                finca_id=orden.finca_id.id
                            ).first()
                        tmp_enfunde_cantidad = enfunde.cantidad if(enfunde) else 0
                        total = tmp_enfunde_cantidad
                        total_final = tmp_enfunde_cantidad
                        racimo_gen = RacimoControl.objects.all().filter(
                            orden_corte_id__semana=orden.semana,
                            orden_corte_id__anio=orden.anio,
                            edad=i,
                            rechazo=False).first()
                        total_final = total_final - \
                            racimo_gen.cantidad if(racimo_gen) else total_final
                        contador = 0
                        contador_semana = 1
                        if(bandera):
                            for j in reversed(range(8, i)):
                                racimo = RacimoControl.objects.all().filter(
                                    orden_corte_id__semana=orden.semana-contador,
                                    orden_corte_id__anio=orden.anio,
                                    edad=j,
                                    rechazo=False).first()
                                total = total-racimo.cantidad if(racimo) else total
                                total_final = total_final - \
                                    racimo.cantidad if(racimo) else total_final
                        else:
                            for j in reversed(range(8, i)):
                                if(orden.semana-contador_semana > 0):
                                    """ if(tmp_contador==16):
                                        print("normal") """
                                    racimo = RacimoControl.objects.all().filter(
                                        orden_corte_id__semana=orden.semana-contador_semana,
                                        orden_corte_id__anio=orden.anio,
                                        edad=j,
                                        rechazo=False).first()
                                    if racimo:
                                        total = total-racimo.cantidad
                                        total_final = total_final-racimo.cantidad
                                        """ if(tmp_contador==17):
                                            print(str(racimo.cantidad)+"-"+str(orden.semana-contador_semana)+"-"+str(orden.anio)+"-"+str(j)) """
                                else:
                                    tmp_semana = 52-(contador_semana-orden.semana)
                                    """ if(tmp_contador==16):
                                        print("no normal: "+str(tmp_semana)) """
                                    racimo = RacimoControl.objects.all().filter(
                                        orden_corte_id__semana=tmp_semana,
                                        orden_corte_id__anio=orden.anio-1,
                                        edad=j,
                                        rechazo=False).first()
                                    if racimo:
                                        total = total-racimo.cantidad
                                        total_final = total_final-racimo.cantidad
                                        """ if(tmp_contador==17):
                                            print(str(racimo.cantidad)+"-"+str(tmp_semana)+"-"+str(orden.anio-1)+"-"+str(j)) """
                                contador_semana = contador_semana+1
                                contador = contador+1
                        """ if(tmp_contador==17):
                            print("Resultado: "+str(orden.anio)+"-"+str(orden.semana)+"-"+str(total)+"-"+str(total_final)) """
                        total_inventario_inicial += total
                        total_inventario_final += total_final
                        """ datos_inventario_inicial[str(i)]=total
                        datos_inventario_final[str(i)]=total_final """
                        if(i == 14):
                            tmp_inventario_final = total_final
                        tmp_data_inventario = {
                            'cinta': racimo_gen.controlcolor_id.color if(racimo_gen) else "",
                            'edad': i,
                            'inicial': total,
                            'final': total_final
                        }
                        datos_inventarios.append(tmp_data_inventario)
                    inventario['inventarios'] = datos_inventarios
                    inventario['total_inicial'] = total_inventario_inicial
                    inventario['total_final'] = total_inventario_final
                    datos['inventario'] = inventario
                    """ datos['inventario_inicial']=datos_inventario_inicial
                    datos['inventario_final']=datos_inventario_final """
                    semana = 0
                    anio = 0
                    if(orden.semana-14 > 0):
                        semana = orden.semana-14
                        anio = orden.anio
                    else:
                        anio = orden.anio-1
                        semana = 52-(14-orden.semana)
                    if(tmp_contador != 0):
                        enfunde = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                        datos['racimos']['recobro'] = round(
                            (1-(tmp_inventario_final/enfunde.cantidad))*100) if(enfunde) else 0

                    # Tendencia
                    tendencia = {}
                    tendencias = []
                    tendencia_total_racimo = 0
                    tendencia_semana = {}
                    tendencia_total_cajas = 0
                    for i in range(8, 15):
                        semana_tmp = orden.semana-1 if(orden.semana-1 > 0) else 52
                        semana = 0
                        tmp_total = 0
                        anio = 0
                        if((orden.semana-i) > 0):
                            semana = orden.semana-i
                            anio = orden.anio
                            enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                            enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=semana_tmp,
                                orden_corte_id__anio=anio,
                                edad=i,
                                rechazo=False).first()
                        else:
                            semana = 52-(i-orden.semana)
                            anio = orden.anio-1
                            enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                            enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=semana_tmp,
                                orden_corte_id__anio=orden.anio if(
                                    orden.semana-1 > 0) else orden.anio-1,
                                edad=i,
                                rechazo=False).first()
                        if(racimo and enfunde_a and enfunde_b):
                            tmp_total = (
                                (racimo.cantidad/enfunde_b.cantidad)*enfunde_a.cantidad)
                        else:
                            """ print("-----------")
                            print("Resumen: "+str(semana)+"-"+str(anio)+"-"+str(i))
                            print("Semana: "+str(orden.semana))
                            print("racimo: "+str(racimo))
                            print("enfunde_a: "+str(enfunde_a))
                            print("enfunde_b: "+str(enfunde_b)) """
                        # tendencia_semana[str(i)]=round(tmp_total,0)
                        tendencia_semana = {
                            'cinta': racimo.controlcolor_id.color if(racimo) else "",
                            'edad': i,
                            'cantidad': round(tmp_total, 0)
                        }
                        tendencias.append(tendencia_semana)
                        tendencia_total_racimo += tmp_total

                    if tmp_contador > 0:
                        tendencia_total_cajas = tendencia_total_racimo * \
                            resultado[tmp_contador-1]['racimos']['ratio']
                        datos['tendencia'] = tendencia
                    tendencia['tendencias'] = tendencias
                    tendencia['total_racimos'] = round(tendencia_total_racimo, 0)
                    tendencia['total_cajas'] = round(tendencia_total_cajas, 0)
                    tmp_contador = tmp_contador+1
                    resultado.append(datos)

                resultado_finca.append(resultado)
            return Response(
            {'mensaje': 'Consulta exitosa',
             'status': 'sucess',
             'data': resultado_finca},)
        return Response(
            {'mensaje': 'Error en el servicio',
             'status': 'error',
             'data': {}},)


class EstadisticaInventario(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """

    def get(self, request, anio, sem_desde, sem_hasta):
        resultado = []
        labels = []
        tmp_contador = 0
        datos_inventario = {
            "8": [],
            "9": [],
            "10": [],
            "11": [],
            "12": [],
            "13": [],
            "14": [],
        }  # finca_id__productor_id__user__id=request.user.id
        ordenes_corte = OrdenCorte.objects.all().order_by('anio', 'semana').filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta)
        for orden in ordenes_corte:
            # Inventarios
            labels.append(orden.semana)
            inventario = {}
            datos_inventarios = []
            total_inventario_inicial = 0
            total_inventario_final = 0
            for i in range(8, 15):
                semana = 0
                bandera = True
                if((orden.semana-i) > 0):
                    semana = orden.semana-i
                    enfunde = Enfunde.objects.all().filter(
                        semana=semana,
                        anio=orden.anio,
                        finca_id=orden.finca_id.id).first()
                else:
                    semana = 52-(i-orden.semana)
                    bandera = False
                    enfunde = Enfunde.objects.all().filter(
                        semana=semana,
                        anio=orden.anio-1,
                        finca_id=orden.finca_id.id
                    ).first()
                tmp_enfunde_cantidad = enfunde.cantidad if(enfunde) else 0
                total = tmp_enfunde_cantidad
                total_final = tmp_enfunde_cantidad
                racimo_gen = RacimoControl.objects.all().filter(
                    orden_corte_id__semana=orden.semana,
                    orden_corte_id__anio=orden.anio,
                    edad=i,
                    rechazo=False).first()
                total_final = total_final - \
                    racimo_gen.cantidad if(racimo_gen) else total_final
                contador = 0
                contador_semana = 1
                if(bandera):
                    for j in reversed(range(8, i)):
                        racimo = RacimoControl.objects.all().filter(
                            orden_corte_id__semana=orden.semana-contador,
                            orden_corte_id__anio=orden.anio,
                            edad=j,
                            rechazo=False).first()
                        total = total-racimo.cantidad if(racimo) else total
                        total_final = total_final - \
                            racimo.cantidad if(racimo) else total_final
                else:
                    for j in reversed(range(8, i)):
                        if(orden.semana-contador_semana > 0):
                            """ if(tmp_contador==16):
                                print("normal") """
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=orden.semana-contador_semana,
                                orden_corte_id__anio=orden.anio,
                                edad=j,
                                rechazo=False).first()
                            if racimo:
                                total = total-racimo.cantidad
                                total_final = total_final-racimo.cantidad
                        else:
                            tmp_semana = 52-(contador_semana-orden.semana)
                            racimo = RacimoControl.objects.all().filter(
                                orden_corte_id__semana=tmp_semana,
                                orden_corte_id__anio=orden.anio-1,
                                edad=j,
                                rechazo=False).first()
                            if racimo:
                                total = total-racimo.cantidad
                                total_final = total_final-racimo.cantidad
                        contador_semana = contador_semana+1
                        contador = contador+1
                total_inventario_inicial += total
                total_inventario_final += total_final
                if(i == 8):
                    datos_inventario["8"].append(total_final)
                elif(i == 9):
                    datos_inventario["9"].append(total_final)
                elif(i == 10):
                    datos_inventario["10"].append(total_final)
                elif(i == 11):
                    datos_inventario["11"].append(total_final)
                elif(i == 12):
                    datos_inventario["12"].append(total_final)
                elif(i == 13):
                    datos_inventario["13"].append(total_final)
                else:
                    datos_inventario["14"].append(total_final)

        return Response(
            {'mensaje': 'Solicitud exitosa',
             'status': 'success',
                'labels': labels,
                "datos": datos_inventario}
        )


class EstadisticaCosecha(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """

    def get(self, request, anio, sem_desde, sem_hasta):
        labels = []
        datos_racimos = {
            "8": [],
            "9": [],
            "10": [],
            "11": [],
            "12": [],
            "13": [],
            "14": [],
        }
        ordenes_corte = OrdenCorte.objects.all().order_by('anio', 'semana').filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta)
        for orden in ordenes_corte:
            labels.append(str(orden.anio)+"-"+str(orden.semana))
            # Racimos
            for i in range(8, 15):
                racimo = RacimoControl.objects.all().filter(
                    orden_corte_id=orden.id, rechazo=False, edad=i).first()
                if(i == 8):
                    datos_racimos["8"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["8"].append(0)
                elif(i == 9):
                    datos_racimos["9"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["9"].append(0)
                elif(i == 10):
                    datos_racimos["10"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["10"].append(0)
                elif(i == 11):
                    datos_racimos["11"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["11"].append(0)
                elif(i == 12):
                    datos_racimos["12"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["12"].append(0)
                elif(i == 13):
                    datos_racimos["13"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["13"].append(0)
                else:
                    datos_racimos["14"].append(racimo.cantidad) if(
                        racimo) else datos_racimos["14"].append(0)

        return Response(
            {'mensaje': 'Solicitud exitosa',
             'status': 'success',
                'labels': labels,
                "datos": datos_racimos}
        )


class EstadisticaCajas(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """

    def get(self, request, anio, sem_desde, sem_hasta):
        labels = []
        datos_cajas = []
        ordenes_corte = OrdenCorte.objects.all().order_by('anio', 'semana').filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta)
        for orden in ordenes_corte:
            labels.append(str(orden.anio)+"-"+str(orden.semana))
            # Racimos
            racimo = CajaControl.objects.filter(
                orden_corte_id=orden.id).first()
            datos_cajas.append(racimo.cantidad) if(
                racimo) else datos_cajas.append(0)

        return Response(
            {'mensaje': 'Solicitud exitosa',
             'status': 'success',
                'labels': labels,
                "datos": datos_cajas}
        )


class EstadisticaRatio(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """

    def get(self, request, anio, sem_desde, sem_hasta):
        labels = []
        datos_ratio = []
        ordenes_corte = OrdenCorte.objects.all().order_by('anio', 'semana').filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta)
        for orden in ordenes_corte:
            labels.append(str(orden.anio)+"-"+str(orden.semana))
            caja_controles = CajaControl.objects.all().filter(
                orden_corte_id=orden.id,
                segunda=False
            )
            suma = 0
            data_cajas = {}
            for caja in caja_controles:
                suma += caja.cantidad*caja.tipo_caja_id.peso
            total_cajas = suma/43
            total_racimos = RacimoControl.objects.all().filter(
                orden_corte_id=orden.id,
                rechazo=False
            ).aggregate(Sum('cantidad'))
            ratio = total_cajas / \
                total_racimos['cantidad__sum'] if(
                    total_racimos['cantidad__sum'] != 0) else 0
            datos_ratio.append(round(ratio, 2))
        return Response(
            {'mensaje': 'Solicitud exitosa',
             'status': 'success',
                'labels': labels,
                "datos": datos_ratio}
        )


class EstadisticaTendencia(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """

    def get(self, request, anio, sem_desde, sem_hasta):
        labels = []
        datos_tendencia = {
            "8": [],
            "9": [],
            "10": [],
            "11": [],
            "12": [],
            "13": [],
            "14": [],
        }
        ordenes_corte = OrdenCorte.objects.all().order_by('anio', 'semana').filter(
            anio=anio,
            semana__gte=sem_desde,
            semana__lte=sem_hasta)
        for orden in ordenes_corte:
            labels.append(str(orden.anio)+"-"+str(orden.semana))
            # Tendencia
            tendencia = {}
            tendencias = []
            tendencia_total_racimo = 0
            tendencia_semana = {}
            tendencia_total_cajas = 0
            for i in range(8, 15):
                semana_tmp = orden.semana-1 if(orden.semana-1 > 0) else 52
                semana = 0
                tmp_total = 0
                anio = 0
                if((orden.semana-i) > 0):
                    semana = orden.semana-i
                    anio = orden.anio
                    enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                    enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                    racimo = RacimoControl.objects.all().filter(
                        orden_corte_id__semana=semana_tmp,
                        orden_corte_id__anio=anio,
                        edad=i,
                        rechazo=False).first()
                else:
                    semana = 52-(i-orden.semana)
                    anio = orden.anio-1
                    enfunde_a = Enfunde.objects.all().filter(semana=semana, anio=anio).first()
                    enfunde_b = Enfunde.objects.all().filter(semana=semana-1, anio=anio).first()
                    racimo = RacimoControl.objects.all().filter(
                        orden_corte_id__semana=semana_tmp,
                        orden_corte_id__anio=orden.anio if(
                            orden.semana-1 > 0) else orden.anio-1,
                        edad=i,
                        rechazo=False).first()
                if(racimo and enfunde_a and enfunde_b):
                    tmp_total = (
                        (racimo.cantidad/enfunde_b.cantidad)*enfunde_a.cantidad)
                if(i == 8):
                    datos_tendencia["8"].append(round(tmp_total, 0))
                elif(i == 9):
                    datos_tendencia["9"].append(round(tmp_total, 0))
                elif(i == 10):
                    datos_tendencia["10"].append(round(tmp_total, 0))
                elif(i == 11):
                    datos_tendencia["11"].append(round(tmp_total, 0))
                elif(i == 12):
                    datos_tendencia["12"].append(round(tmp_total, 0))
                elif(i == 13):
                    datos_tendencia["13"].append(round(tmp_total, 0))
                else:
                    datos_tendencia["14"].append(round(tmp_total, 0))
        return Response(
            {'mensaje': 'Solicitud exitosa',
             'status': 'success',
                'labels': labels,
                "datos": datos_tendencia}
        )

