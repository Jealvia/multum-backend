from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from .models import *
from datetime import datetime,date

class RacimoControlSerializer(serializers.ModelSerializer):

    class Meta:
        model = RacimoControl
        fields = ['id','orden_corte_id','controlcolor_id','edad','rechazo','cantidad']
    def create(self, validated_data):
            """
            Create and return a new `RacimoControl` instance, given the validated data.
            """
            return RacimoControl.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `RacimoControl` instance, given the validated data.
            """
            instance.orden_corte_id = validated_data.get('orden_corte_id', instance.orden_corte_id)
            instance.controlcolor_id = validated_data.get('controlcolor_id', instance.controlcolor_id)
            instance.edad = validated_data.get('edad', instance.edad)
            instance.rechazo = validated_data.get('rechazo', instance.rechazo)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.save()
            return instance

class CajaControlSerializer(serializers.ModelSerializer):

    class Meta:
        model = CajaControl
        fields = ['id','orden_corte_id','tipo_caja_id','segunda','cantidad']
    def create(self, validated_data):
            """
            Create and return a new `CajaControl` instance, given the validated data.
            """
            return CajaControl.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `CajaControl` instance, given the validated data.
            """
            instance.orden_corte_id = validated_data.get('orden_corte_id', instance.orden_corte_id)
            instance.tipo_caja_id = validated_data.get('tipo_caja_id', instance.tipo_caja_id)
            instance.segunda = validated_data.get('segunda', instance.segunda)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.save()
            return instance

