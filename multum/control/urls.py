from django.conf.urls import url

from .views import *

app_name = 'control'

urlpatterns = [
    url(r'^guardar-racimo-control/$', RegistroRacimoControl.as_view()),
    url(r'^guardar-caja-control/$', RegistroCajaControl.as_view()),
    url(r'^produccion/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/finca=(?P<finca>\w+)/todos=(?P<todos>\w+)/$',
        ProduccionDatos.as_view()),
    url(r'^estadistica-inventario/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/$',
        EstadisticaInventario.as_view()),
    url(r'^estadistica-cosecha/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/$',
        EstadisticaCosecha.as_view()),
    url(r'^estadistica-caja/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/$',
        EstadisticaCajas.as_view()),
    url(r'^estadistica-ratio/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/$',
        EstadisticaRatio.as_view()),
    url(r'^estadistica-tendencia/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/$',
        EstadisticaTendencia.as_view()),
]
