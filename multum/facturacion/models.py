from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import Persona
from envio.models import OrdenCorte,MarcaCaja
# Create your models here.

class FacturaCabecera(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    productor_id=models.ForeignKey(Persona,
    related_name='productor_id',on_delete=False)
    fecha_factura = models.DateTimeField()
    estado_factura=models.CharField(max_length=5,blank=True)
    clave_acceso=models.CharField(max_length=50,blank=True)
    autorizacion=models.CharField(max_length=50,blank=True)
    fecha_autorizacion = models.DateTimeField()
    subtotal=models.IntegerField()
    valor_iva=models.IntegerField()
    valor_iva_cero=models.IntegerField()
    total=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "FacturaCabecera"
        verbose_name_plural = "FacturaCabeceras"

    def __str__(self):
        return str(self.id)+"-"+str(self.total)

    def get_absolute_url(self):
        return reverse("FacturaCabecera_detail", kwargs={"pk": self.pk})

class FacturaDetalle(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    cantidad=models.IntegerField()
    precio=models.IntegerField()
    valor=models.IntegerField()
    valor_iva=models.IntegerField()
    con_iva = models.BooleanField(default=True)

    factura_cabecera_id=models.ForeignKey(FacturaCabecera,
    related_name='fd_factura_cabecera_id',on_delete=False)
    orden_corte_id=models.ForeignKey(OrdenCorte,
    related_name='fd_orden_corte_id',on_delete=False)
    marca_caja_id=models.ForeignKey(MarcaCaja,
    related_name='fd_marca_caja_id',on_delete=False)

    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "FacturaDetalle"
        verbose_name_plural = "FacturaDetalles"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("FacturaDetalle_detail", kwargs={"pk": self.pk})