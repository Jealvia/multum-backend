# Generated by Django 2.2.7 on 2020-03-16 14:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('envio', '0016_auto_20200316_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='racimocontrol',
            name='edad',
            field=models.IntegerField(null=True),
        ),
    ]
