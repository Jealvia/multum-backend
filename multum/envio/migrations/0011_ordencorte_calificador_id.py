# Generated by Django 2.2.7 on 2020-03-05 14:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maestros', '0009_auto_20200227_1633'),
        ('envio', '0010_auto_20200227_1633'),
    ]

    operations = [
        migrations.AddField(
            model_name='ordencorte',
            name='calificador_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=False, related_name='oc_persona_id', to='maestros.Persona'),
        ),
    ]
