# Generated by Django 2.2.7 on 2020-02-11 20:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('envio', '0006_auto_20200207_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='embarque',
            name='cantidad_actual',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
