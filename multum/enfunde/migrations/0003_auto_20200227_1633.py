# Generated by Django 2.2.7 on 2020-02-27 16:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enfunde', '0002_auto_20200110_0309'),
    ]

    operations = [
        migrations.RenameField(
            model_name='calibraje',
            old_name='calibraje',
            new_name='cantidad',
        ),
        migrations.RenameField(
            model_name='enfunde',
            old_name='enfunde',
            new_name='cantidad',
        ),
        migrations.RenameField(
            model_name='tipocalibraje',
            old_name='tipo_calibraje',
            new_name='codigo',
        ),
        migrations.RenameField(
            model_name='tipoenfunde',
            old_name='tipo_enfunde',
            new_name='nombre',
        ),
        migrations.AddField(
            model_name='tipocalibraje',
            name='nombre',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
