from rest_framework.parsers import JSONParser,FormParser
from rest_framework import generics,status,viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework_jwt.settings import api_settings
from rest_framework.authtoken.models import Token
#import jwt
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404,render,redirect
from django.http import Http404,JsonResponse,HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .serializers import *
import random as rd
import os
import time
import json
from django.db.models import Subquery,F,Func
from django.db.models.functions import *
import math

from datetime import datetime,date
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import Q

from fcm_django.models import FCMDevice
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser

from .models import *

#Modelos para el movil

class AgregarEnfunde(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        fecha_enfunde = body['fecha_enfunde']
        cinta_id = body['cinta_id']
        finca_id= body['finca_id']
        enfunde= body['enfunde']
        tipo_enfunde_id= body['tipo_enfunde_id']
        id= body['id']
        #Logica de negocio
        cinta=Cinta.objects.all().filter(
            id=cinta_id
        ).first()
        datos={
            'finca_id':finca_id,
            'tipo_enfunde_id':tipo_enfunde_id,
            'cinta_id':cinta_id,
            'fecha_enfunde':fecha_enfunde,
            'anio':cinta.anio,
            'semana':cinta.semana,
            'cantidad':enfunde
        }
        if id==0:
            serializer = EnfundeSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)
        else:
            enfunde=Enfunde.objects.all().filter(id=id).first()
            serializer = EnfundeSerializer(enfunde,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)

class BuscarEnfundeCinta(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        fecha_enfunde = body['fecha_enfunde']
        #Logica negocio
        var="0:0:0-05"
        lol=fecha_enfunde.split("T")
        cadena_busqueda=lol[0]+"T"+var
        cinta=Cinta.objects.all().filter(
            fecha_desde__lte=cadena_busqueda,
            fecha_hasta__gte=cadena_busqueda
        ).first()
        
        serializer=CintaSerializer(cinta,many=False)
        return JsonResponse(serializer.data, safe = False)

class TipoEnfundeList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = TipoEnfunde.objects.all().order_by('nombre')
        serializer = TipoEnfundeSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class TipoCalibrajeList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = TipoCalibraje.objects.all().order_by('nombre')
        serializer = TipoCalibrajeSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class RegistrarCalibraje(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        fecha_calibraje = body['fecha_calibraje']
        finca_id= body['finca_id']
        anio= body['anio']
        semana= body['semana']
        cantidad= body['cantidad']
        cinta_id= body['cinta_id']
        tipo_calibraje_id= body['tipo_calibraje_id']
        id= body['id']
        #Logica de negocio
        datos={
            'finca_id':finca_id,
            'tipo_calibraje_id':tipo_calibraje_id,
            'fecha_calibraje':fecha_calibraje,
            'anio':anio,
            'cinta_id':cinta_id,
            'semana':semana,
            'cantidad':cantidad
        }
        if id==0:
            serializer = CalibrajeSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)
        else:
            calibraje=Calibraje.objects.all().filter(id=id).first()
            serializer = CalibrajeSerializer(calibraje,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)

class CalibrajeListMovil(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request,anio,sem_desde,sem_hasta,finca,todos):
        if todos=="true":
            queryset = Calibraje.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-created_date')
            serializer = CalibrajeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        elif todos=="false":
            queryset = Calibraje.objects.all().filter(
                finca_id=finca,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-created_date')
            serializer = CalibrajeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        else:
            queryset = Calibraje.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
            ).order_by('-created_date')
            serializer = CalibrajeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)

class EnfundeListMovil(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    def get(self, request,anio,sem_desde,sem_hasta,finca,todos):
        if todos=="true":
            queryset = Enfunde.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-created_date')
            serializer = EnfundeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        elif todos=="false":
            queryset = Enfunde.objects.all().filter(
                finca_id=finca,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-created_date')
            serializer = EnfundeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        else:
            queryset = Enfunde.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
            ).order_by('-created_date')
            serializer = EnfundeListMovilSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)

#Modelos para la web
class EnfundeListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if anio!="null":
            if todos=="true":
                queryset = Enfunde.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta)
                serializer = EnfundeListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Enfunde.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta)
                serializer = EnfundeListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = Enfunde.objects.all()
                serializer = EnfundeListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Enfunde.objects.all()
                serializer = EnfundeListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)

class CalibrajeListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if anio!="null":
            if todos=="true":
                queryset = Calibraje.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta)
                serializer = CalibrajeListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Calibraje.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta)
                serializer = CalibrajeListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = Calibraje.objects.all()
                serializer = CalibrajeListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Calibraje.objects.all()
                serializer = CalibrajeListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)


